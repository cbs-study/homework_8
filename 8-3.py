# Створіть список товарів в інтернет-магазині. Серіалізуйте його за допомогою pickle та збережіть у JSON.

import pickle
import json

list_of_goods = [
    {1: "Notebook", "$": 1.0, "quantity": 80},
    {2: "Pen", "$": 0.5, "quantity": 100},
    {3: "Pencil", "$": 0.3, "quantity": 100},
    {4: "Scissors", "$": 2, "quantity": 40},
    {5: "Felt-tip pens", "$": 5, "quantity": 10},
    {6: "Flip chart", "$": 15, "quantity": 5},
]

"""Serialization of the list of goods"""
with open("goods.pickle", "wb") as f:
    pickle.dump(list_of_goods, f)

"""Saving"""
with open("goods.json", "w") as f:
    json.dump(list_of_goods, f)

print("The list of goods has been saved!")
