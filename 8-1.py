# Напишіть скрипт, який створює текстовий файл і записує до нього 10000 випадкових дійсних чисел.
# Створіть ще один скрипт, який читає числа з файлу та виводить на екран їхню суму.

import random
# Open the file for writing
with open("real_numbers.txt", "w") as file:
    for i in range(10000):
        number = random.uniform(0, 1000)  # A random real number between 0 and 1000
        file.write(f"{number}\n")

print("File successfully created!")

# Open the file for reading
with open("real_numbers.txt", "r") as file:
    total_sum = 0
    # Read each string and add the number to the total sum
    for line in file:
        total_sum += float(line)

print("Total sum of random real numbers:", total_sum)
