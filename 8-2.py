# Модифікуйте вихідний код сервісу зі скорочення посилань з ДЗ 7 заняття курсу Python Starter так, щоб він зберігав
# базу посилань на диску і не «забув» при перезапуску. За бажанням можете ознайомитися з модулем shelve
# (https://docs.python.org/3/library/shelve.html), який у даному випадку буде дуже зручним та
# спростить виконання завдання.

import shelve

with shelve.open("link_base") as db:
    while True:
        print("1. Get an initial url: ")
        print("2. Create a short url: ")
        print("3. Exit")
        option = input("Enter your choice: ")
        if option == "1":
            initial_url = input("Enter the initial url: ")
            short_name = input("Enter a short name: ")

            if short_name not in db:
                db[short_name] = initial_url
                print("The url has been created: ", short_name)

        elif option == "2":
            short_name = input("Enter a short name: ")

            if short_name in db:
                initial_url = db[short_name]
                print(f"The origin url is: {initial_url}")
            else:
                print("There is no such url.")

        elif option == "3":
            print("See you nest time!")
            break
        else:
            print("Invalid option")

    db.sync()
    db.close()
